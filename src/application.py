from sqlite3.dbapi2 import Cursor
import db
from types import MethodType
from flask import Flask, request, jsonify
from flask_cors import CORS
import os


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(SECRET_KEY='dev', DATABASE=os.path.join(app.instance_path, 'database.sqlite'), )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    return app


app = create_app()
db.init_app(app)
CORS(app)


@app.route('/ManageInfo', methods=( "GET","POST"))
def savedata():
    json = request.json
    nombre = json["Nombre"]
    apellido = json["Apellido"]
    edad = json["Edad"]
    estado = json["Estado"]
    pais = json["Pais"]
    
    if request.method == "POST":
        database = db.get_db()
        database.execute('INSERT INTO user (nombre, apellido, edad, estado, pais) VALUES (?, ?, ?, ?, ?)',
        (nombre, apellido, edad, estado, pais))
        database.commit()
        return {"Success": True}
    else:
        return "Error"


@app.route('/TableData', methods=("GET", "POST", "PUT", "DELETE"))
def showinfo():
    if request.method == "GET":
        database = db.get_db()
        cursor = database.cursor()
        cursor.execute('SELECT * FROM user')
        rows = cursor.fetchall()

        listobject = []

        for Info in rows:
            listobject.append({"Id": Info["id"], "Nombre": Info["nombre"], "Apellido": Info["apellido"], "Edad": Info["edad"], "Estado": Info["estado"], "Pais": Info["pais"]})

        return jsonify(listobject)
    
    if request.method == "PUT":
            json = request.json
            database = db.get_db()
            cursor = database.cursor()
            cursor.execute("UPDATE user SET nombre = " + "'" + json["Nombre"] + "'"+ ", apellido = "+ "'" +json["Apellido"]+ "'" + ", edad = "+ str(json["Edad"]) + ",estado = "+ "'" +json["Estado"]+ "'" +", pais = "+"'" +json["Pais"] + "'" +" WHERE id =" + str(json["Id"]))
            database.commit()
            cursor.execute("SELECT * FROM user WHERE Id = " +str(json["Id"]))
            rows = cursor.fetchone()
            record = {"Id": rows["id"], "Nombre": rows["nombre"], "Apellido": rows["apellido"], "Edad": rows["edad"], "Estado": rows["estado"], "Pais": rows["pais"]}
            jsonObject = { "record": record, "Success": True}
            return jsonObject
    if (request.method== "DELETE"):
            json = request.json
            database = db.get_db()
            cursor = database.cursor()
            cursor.execute("DELETE from user WHERE id = " + str(json["Id"]))
            database.commit()
            return {"Success": True}

 