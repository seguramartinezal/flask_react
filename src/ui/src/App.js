import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css'
import Table from "./TableData";
import ManageInfo from "./ManageInfo";
import Home from "./Home";

function App() {
  return (
    <div className="App">
    <Layaout/>
    </div>
  );
}

export default App;

function Layaout(){

  return (<div>

    <Router>

      <Redirect to="/Home" from="Layaout"/>

      <Switch>
        <Route exact path="/ManageInfo" component={ManageInfo}/>
        <Route exact path="/TableData" component={Table}/>
        <Route exact path="/Home" component={Home}/>
      </Switch>

    </Router>



  </div>)
}