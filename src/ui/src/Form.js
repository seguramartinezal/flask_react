import 'bootstrap/dist/css/bootstrap.css'
import {
    Link
} from "react-router-dom";
import './ProjectStyle.css'

export default function Formulario({SendForm,Handle}){

    return (

        <form className="Container" onSubmit={SendForm}>
            <div className="jumbotron"><h2>Formulario</h2></div>
            <div className="form-group row">
                <label htmlFor="Name" className="col-sm-1 col-form-label col-form-label-sm">Nombre</label>
                <div className="col-sm-2">
                    <input type="text" className="form-control form-control-sm" name="Nombre"
                           onChange={Handle} placeholder="Introduce un Nombre" required/>
                </div>
            </div>

            <div className="form-group row">
                <label htmlFor="text" className="col-sm-1 col-form-label col-form-label-sm">Apellido</label>
                <div className="col-sm-2">
                    <input type="Apellido" className="form-control form-control-sm" name="Apellido"
                           onChange={Handle} placeholder="Introduce un Apellido" required/>
                </div>
            </div>


            <div className="form-group row">
                <label htmlFor="text" className="col-sm-1 col-form-label col-form-label-sm">Edad</label>
                <div className="col-sm-2">
                    <input type="Edad" className="form-control form-control-sm" name="Edad"
                           onChange={Handle} placeholder="Introduce una Edad" required/>
                </div>
            </div>


            <div className="form-group row">
                <label htmlFor="Estado" className="col-sm-1 col-form-label col-form-label-sm">Estado</label>
                <div className="col-sm-2">
                    <input type="text" className="form-control form-control-sm" name="Estado"
                           onChange={Handle} placeholder="Introduce un Estado" required/>
                </div>
            </div>

            <div className="form-group row">
                <label htmlFor="Pais" className="col-sm-1 col-form-label col-form-label-sm">Pais</label>
                <div className="col-sm-2">
                    <input type="text" className="form-control form-control-sm" name="Pais"
                           onChange={Handle}  placeholder="Introduce un Pais" required/>
                </div>
            </div>

            <div className="form-group row">
                <div className="col-sm-4">
                    <button value="submit" className="btn-success">Enviar</button>
                    <br/>
                    <Link to="/Home"><button className="btn-info">Volver</button> </Link>
                    <br/>
                    <Link to="/TableData"><button className="btn-btn btn-light" id="VerTabla">Ver Tabla</button> </Link>
                </div>
            </div>
        </form>
    )
}