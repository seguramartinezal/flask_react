import 'bootstrap/dist/css/bootstrap.css';
import {
    Link
} from "react-router-dom";
import './ProjectStyle.css'
import React from "react";
import axios from 'axios';
import {Button, Form, Modal} from 'react-bootstrap';
import {GoSearch} from "react-icons/all";
import {NotificationContainer, NotificationManager} from 'react-notifications';
import user from "./user.png";

export default class Table extends React.Component {

    constructor(props) {
        super(props);
        this.state = {data: [],
            Open: false,
            Id: 0,
            nombre: "",
            apellido: "",
            edad: 0,
            estado: "",
            pais: "",
            valueState: false,
            idValue: false,
            valueButton: "",
            datacopy: []}
        this.handleModalShowHide = this.handleModalShowHide.bind(this);
        this.openModal = this.openModal.bind(this);
        this.sendUpdateData= this.sendUpdateData.bind(this);
    }

    URLTableInfo = "http://127.0.0.1:5000/TableData";

    componentDidMount() {
        axios.get(this.URLTableInfo)
            .then(res => {
                const rows = res.data;
                this.setState({data: rows, datacopy: rows})
            })
    }


    handleModalShowHide(obj) {
        this.setState({Id: obj.Id, nombre: obj.Nombre, apellido:obj.Apellido, edad: obj.Edad, estado: obj.Estado, pais: obj.Pais})
        this.setState({Open: !this.state.Open})

    }

    openModal(){
        this.setState({Open: !this.state.Open})
    }

    handleData = ({target}) => {
        const {name, value} = target;
        this.setState({[name]: value})
    }

    createNotification = () => {

        if (this.state.valueState){
            NotificationManager.info("Actualizacion con exito", "Tabla", 500);
            this.setState({valueState: null})
        }
        if (this.state.idValue){
            NotificationManager.success("Registro borrado", "Tabla", 500);
            this.setState({idValue: null})
        }
        else if (this.state.idValue || this.state.valueState)
            NotificationManager.error("Error interno", "Tabla", 500);

    };


    getValueButton = (event) => {
        this.setState({valueButton: event.target.value})
    }

    operations = (event) => {
        event.preventDefault();
        let button = this.state.valueButton;

        if (button === "Guardar"){
            this.sendUpdateData(event);
        }
        if (button === "Borrar") {
            this.deleteData(event);
        }
       /* if (button === "Filtrar"){
            this.filtertest(event);
        }*/
    }


    sendUpdateData = async (event) => {
        event.preventDefault();
            let data = {
                Id: this.state.Id,
                Nombre: this.state.nombre,
                Apellido: this.state.apellido,
                Estado: this.state.estado,
                Edad: this.state.edad,
                Pais: this.state.pais
            }

            let valor = await axios.put(this.URLTableInfo, data);
            this.setState({valueState: valor.data["Success"]});
            const id = valor.data["record"].Id;
            const array = this.state.data.filter((record) => record.Id !== id);
            array.push(valor.data["record"]);

            function compare(obj1, obj2) {
                if (obj1.Id > obj2.Id) {
                    return 1;
                } else if (obj2.Id > obj1.Id) {
                    return -1;
                } else {
                    return 0;
                }
            }

            array.sort(compare);
            this.setState({data: array, datacopy: array});
            this.createNotification();
            this.openModal();

    }

    deleteData = async (event) => {
        event.preventDefault();
        let valor = await axios.delete(this.URLTableInfo, {data: {'Id':this.state.Id}});
        this.setState({idValue: valor.data["Success"]});
        const array = this.state.data.filter((record) => record.Id !== this.state.Id);
        this.setState({data: array, datacopy: array})
        this.createNotification();
        this.openModal();
    }

    filterdata = (event) => {
        event.preventDefault();

        const array = this.state.data.filter((record) => record.Nombre === this.state.nombre);
        this.setState({data: array});
    }

    filterdata = (event) => {
        event.preventDefault();
        let val = document.getElementById("filter");
        let filter = val.value.toLowerCase();

        if (filter){
        const array = this.state.data.filter((record) => record.Nombre.toLowerCase().includes(filter) ||
        record.Apellido.toLowerCase().includes(filter) || record.Estado.toLowerCase().includes(filter)
        || record.Pais.toLowerCase().includes(filter) || record.Edad.toString().includes(filter));
        this.setState({data: array});
        }else{
        const array = this.state.datacopy;
        this.setState({data: array});
        }

    }

    render() {
        return (
            <div>
            <img src={user} alt="user" id="img"/>
                <h3 id="title"><i>Tabla de Clientes</i></h3>
            <Form id="containerForm" >
                {/*<input type="text" className="col-md-4" onChange={this.handleData} name="nombre" placeholder="Inserte datos para filtrar" />*/}
                <input type="text" className="col-md-4"  onKeyUp={this.filterdata} id="filter" placeholder="Inserte datos para filtrar" />
                <Button variant="primary" id="Button" type="submit" value="Filtrar" onClick={this.getValueButton}><GoSearch/></Button>
            </Form>

                <div id = "containertable">

                <table id="table" className="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Edad</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Pais</th>

                    </tr>
                    </thead>
                    <tbody>

                    {this.state.data.map((obj) => <tr>

                        <td onClick={() => this.handleModalShowHide(obj)}>{obj.Id}</td>
                        <td onClick={() => this.handleModalShowHide(obj)}>{obj.Nombre}</td>
                        <td onClick={() => this.handleModalShowHide(obj)}>{obj.Apellido}</td>
                        <td onClick={() => this.handleModalShowHide(obj)}>{obj.Edad}</td>
                        <td onClick={() => this.handleModalShowHide(obj)}>{obj.Estado}</td>
                        <td onClick={() => this.handleModalShowHide(obj)}>{obj.Pais}</td>

                    </tr>)}

                    </tbody>
                    <Link to="/Home">
                        <button className="btn-info" id="VolverForm">Volver</button>
                    </Link>

                </table>

                <Modal show={this.state.Open}>
                    <Modal.Header closeButton onClick={this.openModal}>
                        <p className="text-center"><Modal.Title>Formulario</Modal.Title></p>
                    </Modal.Header>
                    <Modal.Body>

                        <Form className="form-group" onSubmit={this.operations} >

                            <input type="text" className="form-control" placeholder="Nombre" value={this.state.nombre} name="nombre" onChange={this.handleData}/><br/>
                            <input type="text" className="form-control" placeholder="Apellido" value={this.state.apellido} name="apellido"  onChange={this.handleData}/><br/>
                            <input type="text" className="form-control" placeholder="Edad" value={this.state.edad} name="edad"  onChange={this.handleData}/><br/>
                            <input type="text" className="form-control" placeholder="Estado" value={this.state.estado} name="estado"  onChange={this.handleData}/><br/>
                            <input type="text" className="form-control" placeholder="Pais" value={this.state.pais} name="pais"  onChange={this.handleData}/>

                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.openModal}>
                                    Cerrar
                                </Button>
                                <Button variant="primary" onClick={this.getValueButton} type="submit" value="Guardar">
                                   Guardar
                                </Button>
                                 <Button variant="danger" onClick={this.getValueButton} type="submit" value="Borrar">
                                    Borrar
                                </Button>

                            </Modal.Footer>

                        </Form>

                    </Modal.Body>
                </Modal>
                <NotificationContainer/>
            </div>

        </div>
        )

    };
}