import {Link} from "react-router-dom";
import {slide as Menu} from "react-burger-menu";
import {FaWpforms, ImTable} from "react-icons/all";


export default function Home() {

    return (

        <div id="outer-container">

            <Menu right>
             <Link to="/ManageInfo" > <FaWpforms/> Formulario</Link>
             <Link to="/TableData"><ImTable/> Tabla</Link>
            </Menu>

        <main>
        <div>

            <div className="jumbotron"><h2>Bienvenido</h2></div>

        </div>
        </main>


        </div>
    )
}