import React from "react";
import Formulario from "./Form";
import axios from "axios";
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

export default class ManageInfo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            Nombre: "", Apellido: "", Edad: 0, Pais: "", Estado: "", ValueState: ""
        };
        this.HandleArr = this.HandleArr.bind(this);
        this.sendDataAxios = this.sendDataAxios.bind(this);
        this.myChangeHandler = this.myChangeHandler.bind(this);
    }

    myChangeHandler = ({target}) => {
        const {name, value} = target;
        this.setState(() => ({[name]: value}));
    }

    HandleArr = (event) => {
        const URL = "http://127.0.0.1:5000/ManageInfo";
        event.preventDefault();

        let data = {
            Nombre: this.state.Nombre,
            Apellido: this.state.Apellido,
            Estado: this.state.Estado,
            Edad: this.state.Edad,
            Pais: this.state.Pais
        }
        fetch(URL, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'},
        }).then(Response => Response.json())
    }


    createNotification = () => {

       if(this.state.ValueState){
         NotificationManager.success("Envio con exito", "Formulario", 300);
       } else {
         NotificationManager.error("Error interno", "Formulario", 1000);
       }
    };

    clearData = (event) => {
        //Limpiar campos del formulario
        event.preventDefault();
        const form = event.target;
        form.reset();

    }

    sendDataAxios = async (event) => {
        event.preventDefault();
        let data = {
            Nombre: this.state.Nombre,
            Apellido: this.state.Apellido,
            Estado: this.state.Estado,
            Edad: this.state.Edad,
            Pais: this.state.Pais
        }

        const URL = "http://127.0.0.1:5000/ManageInfo";
        let valor = await axios.post(URL, data);
        this.setState({ValueState: valor.data["Success"]});
        this.createNotification();
        this.clearData(event);
    }

    render() {
        return (
            <div>
                <Formulario SendForm={this.sendDataAxios} Handle={this.myChangeHandler} Value={this.HandleArr}/>

                <NotificationContainer/>

            </div>
        )
    };
}